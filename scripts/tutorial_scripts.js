alphabet=new Array('letter-a.png', 'letter-b.png', 'letter-c.png', 'letter-d.png', 'letter-e.png','letter-f.png','letter-g.png', 'letter-h.png',
'letter-i.png', 'letter-j.png', 'letter-k.png', 'letter-l.png', 'letter-m.png', 'letter-n.png', 'letter-o.png', 'letter-p.png', 'letter-q.png', 
'letter-r.png', 'letter-s.png', 'letter-t.png', 'letter-u.png', 'letter-v.png', 'letter-w.png', 'letter-x.png', 'letter-y.png', 'letter-z.png')
classes=new Array('none', 'none', 'none', 'none', 'none', 'none', 'none', 'none', 'none',  'animate_J', 'none', 'none', 'none', 'none', 'none', 'none', 'reposition_Q', 'none', 'none', 'none', 'none', 'none', 'none', 'none', 'none', 'animate_Z')
notes=new Array(
    'Unlike for \'s\', your fingers point straight down.', //a
    'Your thumb can rest against your index finger instead of on your palm.', //b
    'Your palm can face the audience.', //c
    'It looks like a lower-case \'d\' from the side.', //d
    'Your fingers and thumb can be closer together.', //e
    'Not much to say about this letter...', //f
    'Point with your index finger.', //g
    'Point with your index and middle finger.', //h
    'Pinkie out!', //i
    'Start with an \'i\' and draw the letter \'J\'. (Note: The animation has not been fixed in Chrome.)', //j
    'Your middle finger is angled forward and your thumb touches your middle finger.', //k
    'It\'s the shape of a capital \'L\'.', //l
    'It looks like a lower-case \'m\'.', //m
    'It looks like a lower-case \'n\'.', //n
    'It looks like the letter \'o\'.', //o
    'This is basically an upside-down \'k\'.', //p
    'Not much to say about this letter...', //q
    'Cross your fingers!', //r
    'Unlike for \'a\', your hand forms a fist.', //s
    'This is like the letter \'m\' or \'n\', but with your thumb between your index and middle fingers.', //t
    'This is like the letter \'h\', but with your fingers pointed up and your palm facing the audience.', //u
    'This is like the letter \'u\', but with your fingers parted. \n\'V\' for victory!', //v
    'This is like the letter \'v\', but with your ring-finger included.', //w
    'Your index finger should form a hook, like a pirate.', //x
    'If you also point out your index finger, you would be signing \'I love you\'!', //y
    'Draw the letter \'z\' with your index finger.'  //z
)

function showAlphabet(ref){
    var hand = "url('images/Tutorial_Images/"
    
    hand += (alphabet[ref]);
    hand += "\')"
//	document.write(hand)
    document.getElementById('hand').style.backgroundImage = hand;

    document.getElementById('hand').setAttribute('class',classes[ref]);
    document.getElementById('abc_description').innerHTML = notes[ref];
    
    if (ref != 9 && ref != 25)
        document.getElementById('animated').innerHTML = "<span style='color:#C0C0C0;'>...</span>"
    else
        document.getElementById('animated').innerHTML = "<span style='color:#000;'>Click to Animate!</span>"
}
