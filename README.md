This website created for a Women in Information and Computer Sciences (WICS) competition at UC Irvine in Fall 2013. 
The constraints were to only use HTML/CSS and Javascript. (No frameworks)
It was originally hosted on UCI's ICS servers.

We competed against at least 8 other teams, and we won "Best Website."